<?php

namespace Drupal\luzmo_viewer;

use Luzmo\Luzmo;

/**
 *
 */
class LuzmoOverride extends Luzmo {

  private $host;
  private $port = '443';
  private $apiVersion = '0.1.0';
  private $apiKey;
  private $apiToken;
  private $format;

  /**
   * {@inheritDoc}
   */
  public static function initialize($apiKey, $apiToken, $host = 'https://api.luzmo.com', $format = 'array') {
    $instance = new self();
    $instance->apiKey = $apiKey;
    $instance->apiToken = $apiToken;
    $instance->host = $host ? $host : 'https://api.luzmo.com';
    $instance->format = ($format === 'object' || $format === 'array') ? $format : 'array';
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function create($resource, $properties, $associations = [], $json_decode = TRUE) {
    return $this->_emit($resource, 'POST', [
      'action'       => 'create',
      'properties'   => $properties,
      'associations' => $associations,
    ], $json_decode);
  }

  /**
   *
   */
  public function _emit($resource, $action, $query, $json_decode = TRUE) {
    $url = $this->host . ':' . $this->port . '/' . $this->apiVersion . '/' . $resource;

    $query['key'] = $this->apiKey;
    $query['token'] = $this->apiToken;
    $query['version'] = $this->apiVersion;

    try {
      $client = \Drupal::httpClient();
      $request = $client->request($action, $url, [
        'json' => $query,
      ]);

      $response = $request->getBody()->getContents();
      $content_type = $request->getHeaderLine('Content-Type');

      // Check content type and handle accordingly.
      if (strpos($content_type, 'application/json') === FALSE) {
        // Non-JSON response (e.g. image or PDF export) -> return as such.
        return $response;
      }
      else {
        // JSON response from Luzmo API -> decode.
        return json_decode($response, $this->format === 'array');
      }
    }
    catch (\Exception $exception) {
      \Drupal::logger('luzmo_viewer')
        ->critical('Luzmo APi returned an critical error: @message', [
          '@message' => $exception->getMessage(),
        ]);
      return NULL;
    }
  }

}
