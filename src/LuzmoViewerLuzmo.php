<?php

namespace Drupal\luzmo_viewer;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Service description.
 */
class LuzmoViewerLuzmo {

  /**
   * Luzmo inactivity interval.
   *
   * @var int
   */
  public static $inactivity_interval;

  /**
   * Luzmo inactivity interval.
   *
   * @var int
   */
  public static $cache_expiration;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var array
   */
  private array $authorizationSettings;

  /**
   * The Luzmo class.
   *
   * @var \Drupal\luzmo_viewer\LuzmoOverride|\Luzmo\Luzmo
   */
  public $luzmo;

  /**
   * The luzmo configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $user;

  /**
   * Current user settings.
   *
   * @var array
   */
  private $userSettings;

  /**
   * Constructs a LuzmoViewerLuzmo object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory, AccountInterface $user) {
    $this->moduleHandler = $module_handler;
    $this->config = $config_factory->get('luzmo_viewer.settings');
    $this->user = $user;

    if ($user->isAnonymous()) {
      $this->userSettings = $this->config->get('anonymous_account');
    }
    else {
      $this->userSettings = $this->config->get('logged_account');
    }

    self::$inactivity_interval = intval($this->userSettings["inactivity_interval"]);

    if (empty($this->userSettings["cache_expiration"])) {
      self::$cache_expiration = self::$inactivity_interval;
    }
    else {
      self::$cache_expiration = intval($this->userSettings["cache_expiration"]);
    }
  }

  /**
   * Method description.
   */
  public function initialize($authKey, $authToken) {
    $this->luzmo = LuzmoOverride::initialize($authKey, $authToken);

    if ($this->moduleHandler->moduleExists('token')) {
      foreach ($this->userSettings as &$setting) {
        $setting = \Drupal::token()->replace($setting, ['user' => $this->user]);
      }
    }

    $this->authorizationSettings = [
      'type' => 'embed',
      'username' => $this->userSettings["username"],
      'suborganization' => $this->userSettings["suborganization"],
      'name' => $this->userSettings["name"],
      'email' => $this->userSettings["email"],
      'inactivity_interval' => self::$inactivity_interval,
      'role' => 'viewer',
      'access' => [],
    ];

    if (self::$inactivity_interval < 120) {
      unset($this->authorizationSettings['inactivity_interval']);
    }

    return $this;
  }

  /**
   * @return mixed
   */
  public function authorize(): mixed {
    return $this->luzmo->create('authorization', $this->authorizationSettings);
  }

  /**
   *
   */
  public function addAccessRequest($dataType, $settings) {
    $this->authorizationSettings['access'][$dataType][] = $settings;
    return $this;
  }

  /**
   * @return array
   */
  public function getAuthorizationSettings(): array {
    return $this->authorizationSettings;
  }

  /**
   *
   */
  public function setAuthorizationSettings($authorizationSettings): static {
    $this->authorizationSettings = $authorizationSettings;
    return $this;
  }

}
