<?php

namespace Drupal\luzmo_viewer\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;

/**
 * Defines the 'luzmo_viewer_dashboard' field type.
 *
 * @FieldType(
 *   id = "luzmo_viewer_dashboard",
 *   label = @Translation("Luzmo Dashboard"),
 *   category = @Translation("General"),
 *   default_widget = "luzmo_viewer_dashboard",
 *   default_formatter = "luzmo_viewer_dashboard_javascript"
 * )
 *
 * @DCG
 * If you are implementing a single value field type you may want to inherit
 * this class form some of the field type classes provided by Drupal core.
 * Check out /core/lib/Drupal/Core/Field/Plugin/Field/FieldType directory for a
 * list of available field type implementations.
 */
class LuzmoDashboardItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    // @DCG
    // See /core/lib/Drupal/Core/TypedData/Plugin/DataType directory for
    // available data types.
    $properties['dashboard_id'] = DataDefinition::create('string')
      ->setLabel(t('Dashboard ID'))
      ->setRequired(TRUE);

    $properties['settings'] = MapDataDefinition::create()
      ->setLabel(new TranslatableMarkup('Options'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    $columns = [
      'dashboard_id' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'The dashboard ID.',
        'length' => 255,
      ],
      'options' => [
        'description' => 'Serialized array of options for the dashboard.',
        'type' => 'blob',
        'size' => 'big',
        'serialize' => TRUE,
      ],
    ];

    $schema = [
      'columns' => $columns,
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition): array {
    $random = new Random();
    $values['dashboard_id'] = $random->word(mt_rand(1, 50));
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    $value = $this->get('dashboard_id')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints(): array {
    $constraints = parent::getConstraints();

    $constraint_manager = \Drupal::typedDataManager()
      ->getValidationConstraintManager();

    // @DCG Suppose our value must not be longer than 10 characters.
    $options['dashboard_id']['Length']['max'] = 75;

    // @DCG
    // See /core/lib/Drupal/Core/Validation/Plugin/Validation/Constraint
    // directory for available constraints.
    $constraints[] = $constraint_manager->create('ComplexData', $options);
    return $constraints;
  }

}
