<?php

namespace Drupal\luzmo_viewer\Plugin\Field\FieldFormatter;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\luzmo_viewer\LuzmoViewerLuzmo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Luzmo Dashboard Javascript' formatter.
 *
 * @FieldFormatter(
 *   id = "luzmo_viewer_dashboard_javascript",
 *   label = @Translation("Luzmo Dashboard Javascript"),
 *   field_types = {
 *     "luzmo_viewer_dashboard"
 *   }
 * )
 */
class LuzmoDashboardJavascriptFormatter extends FormatterBase {

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $luzmoConfig;

  /**
   * LuzmoViewer Class.
   *
   * @var \Drupal\luzmo_viewer\LuzmoViewerLuzmo
   */
  private LuzmoViewerLuzmo $luzmo;

  /**
   * {@inheritDoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, $configFactory, LuzmoViewerLuzmo $luzmoViewerLuzmo, ModuleHandlerInterface $module_handler, ThemeManagerInterface $theme_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->luzmoConfig = $configFactory->get('luzmo_viewer.settings');
    $this->moduleHandler = $module_handler;
    $this->themeManager = $theme_manager;
    $this->luzmo = $luzmoViewerLuzmo;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return ['require_authorisation' => FALSE] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['label'], $configuration['view_mode'], $configuration['third_party_settings'], $container->get('config.factory'), $container->get('luzmo_viewer.luzmo'), $container->get('module_handler'), $container->get('theme.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];
    $elements['require_authorisation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require an authorissation'),
      '#default_value' => $this->getSetting('require_authorisation'),
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t('require_authorisation: @require_authorisation', ['@require_authorisation' => $this->getSetting('require_authorisation')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $authkey = $this->luzmoConfig->get('authkey');
    $authtoken = $this->luzmoConfig->get('authtoken');

    if (!empty($this->getSetting('require_authorisation'))) {
      $authorisation = $this->luzmoAuthorize($items);
      if (!empty($authorisation["id"]) && !empty($authorisation["token"])) {
        $authkey = $authorisation["id"];
        $authtoken = $authorisation["token"];
      }
    }

    foreach ($items as $delta => $item) {
      $options = $item->options ?: [];
      $attributesArray = [
        'appserver' => $this->luzmoConfig->get('appserver'),
        'authkey' => $authkey,
        'authtoken' => $authtoken,
        'dashboardid' => $item->dashboard_id,
      ] + $options;

      $elements[$delta] = [
        '#theme' => 'luzmo_javascript',
        '#attributes' => new Attribute($attributesArray),
        '#attached' => [
          'library' => [
            'luzmo_viewer/luzmo_viewer',
          ],
        ],
      ];

      if ($this->getSetting('require_authorisation')) {
        $elements[$delta]['#cache'] = [
          'contexts' => ['user'],
          'max-age' => LuzmoViewerLuzmo::$cache_expiration,
        ];
      }
    }

    return $elements;
  }

  /**
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   Array of luzmo field items.
   *
   * @return mixed
   *   Return luzmo authorisation result.
   */
  protected function luzmoAuthorize(FieldItemListInterface $items) {
    $this->luzmo->initialize($this->luzmoConfig->get('authkey'), $this->luzmoConfig->get('authtoken'));

    /*
     * Add access to dashboard item
     */
    $collections = $this->luzmoConfig->get('collections') ?: [];
    foreach ($collections as $collection) {
      $this->luzmo->addAccessRequest('collections', [
        'id' => $collection,
        'inheritRights' => 'read',
      ]);
    }

    $authorizationSettings = $this->luzmo->getAuthorizationSettings();
    foreach ($items as $item) {
      $dashboard_id = $item->dashboard_id;
      // Let module alter dashboard authorisation.
      $this->moduleHandler->alter([
        'luzmo_viewer_dashboard_javascript_authorisation',
      ], $authorizationSettings, $dashboard_id, $this->viewMode);
      // Let themes alter dashboard authorisation.
      $this->themeManager->alter([
        'luzmo_viewer_dashboard_javascript_authorisation',
      ], $authorizationSettings, $dashboard_id, $this->viewMode);
    }
    $this->luzmo->setAuthorizationSettings($authorizationSettings);

    return $this->luzmo->authorize();
  }

}
