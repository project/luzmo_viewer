<?php

namespace Drupal\luzmo_viewer\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'luzmo_viewer_dashboard' field widget.
 *
 * @FieldWidget(
 *   id = "luzmo_viewer_dashboard",
 *   label = @Translation("Luzmo Dashboard"),
 *   field_types = {"luzmo_viewer_dashboard"},
 * )
 */
class LuzmoDashboardWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['dashboard_id'] = [
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]->dashboard_id) ? $items[$delta]->dashboard_id : NULL,
      '#title' => $this->t('Luzmo Dashboard ID'),
    ];

    $element['options'] = [
      '#type' => 'details',
      '#title' => $this->t('Luzmo Dashboard Settings'),
      '#group' => 'settings_group',
    ];

    $element['options']['itemId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Item ID'),
      '#default_value' => isset($items[$delta]->options['itemId']) ? $items[$delta]->options['itemId'] : NULL,
    ];

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    return array_map(function (array $value) {
      if (isset($value['options'])) {
        $value['options'] = array_filter($value['options'], function ($option) {
          return $option !== "";
        });
      }
      return $value;
    }, $values);
  }

}
