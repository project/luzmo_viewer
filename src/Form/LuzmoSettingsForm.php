<?php

namespace Drupal\luzmo_viewer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Luzmo Viewer settings for this site.
 */
class LuzmoSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'luzmo_viewer_luzmo_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#tree'] = TRUE;

    $config = $this->config('luzmo_viewer.settings');
    $form['authkey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('authkey'),
      '#default_value' => $config->get('authkey'),
    ];
    $form['authtoken'] = [
      '#type' => 'textarea',
      '#title' => $this->t('authtoken'),
      '#default_value' => $config->get('authtoken'),
    ];
    $form['luzmo_js'] = [
      '#type' => 'textfield',
      '#title' => $this->t('luzmo.js location'),
      '#default_value' => $config->get('luzmo_js'),
    ];
    $form['appserver'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Luzmo App Server'),
      '#default_value' => $config->get('appserver'),
    ];

    /*
     * Collection ids.
     */
    // Gather the number of collections in the form already.
    $collections_count = $form_state->get('collections');
    $collections_config = $config->get('collections');
    // If there is no collections in the form, use the config count.
    if ($collections_count == NULL && $collections_config !== NULL) {
      $collections_count = count($collections_config);
    }
    // If there is no results in both or count is 0
    // (can append if you remove everything), set count to 1.
    if (!is_int($collections_count) || $collections_count < 1) {
      $collections_count = 1;
    }
    $form_state->set('collections', $collections_count);

    $form['collections'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Collections IDs'),
      '#prefix' => '<div id="collections-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    for ($i = 0; $i < $collections_count; $i++) {
      $form['collections']['collection'][$i] = [
        '#type' => 'textfield',
        '#title' => $this->t('Collection ID'),
        '#default_value' => (!empty($collections_config[$i])) ? $collections_config[$i] : NULL,
      ];
    }

    $form['collections']['actions'] = [
      '#type' => 'actions',
    ];
    $form['collections']['actions']['add_collection'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addOneCollection'],
      '#ajax' => [
        'callback' => '::addMoreCollectionCallback',
        'wrapper' => 'collections-fieldset-wrapper',
      ],
    ];

    /*
     * Logged account.
     */
    $form['logged_account'] = [
      '#type' => 'details',
      '#title' => $this->t('Account settings for logged-in users'),
      '#description' => $this->t('Used for authentification'),
      '#tree' => TRUE,
    ];

    $form['logged_account']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account Username'),
      '#default_value' => $config->get('logged_account')['username'],
    ];

    $form['logged_account']['suborganization'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account Suborganisation'),
      '#default_value' => $config->get('logged_account')['suborganization'],
    ];

    $form['logged_account']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account Name'),
      '#default_value' => $config->get('logged_account')['name'],
    ];

    $form['logged_account']['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account e-mail'),
      '#default_value' => $config->get('logged_account')['email'],
    ];

    $form['logged_account']['inactivity_interval'] = [
      '#type' => 'number',
      '#title' => $this->t('Duration of inactivity (in seconds) after which the token is prematurely invalidated.'),
      '#description' => $this->t('Values under 120 will be used as 0'),
      '#default_value' => $config->get('logged_account')['inactivity_interval'],
    ];

    $form['logged_account']['cache_expiration'] = [
      '#type' => 'number',
      '#title' => $this->t('Field cache'),
      '#description' => $this->t('Duration of the Drupal cache for the field and, by inheritance,the entity.') . '<br/>' . $this->t('Leave empty to use inactivity level.'),
      '#default_value' => (isset($config->get('anonymous_account')['cache_expiration'])) ? $config->get('anonymous_account')['cache_expiration'] : NULL,
    ];

    /*
     * Anonymous account settings.
     */
    $form['anonymous_account'] = [
      '#type' => 'details',
      '#title' => $this->t('Account settings for anonymous users'),
      '#description' => $this->t('Used for authentification'),
      '#tree' => TRUE,
    ];

    $form['anonymous_account']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account Username'),
      '#default_value' => $config->get('anonymous_account')['username'],
    ];

    $form['anonymous_account']['suborganization'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account Suborganisation'),
      '#default_value' => $config->get('anonymous_account')['suborganization'],
    ];

    $form['anonymous_account']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account Name'),
      '#default_value' => $config->get('anonymous_account')['name'],
    ];

    $form['anonymous_account']['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account e-mail'),
      '#default_value' => $config->get('anonymous_account')['email'],
    ];

    $form['anonymous_account']['inactivity_interval'] = [
      '#type' => 'number',
      '#title' => $this->t('Duration of inactivity (in seconds) after which the token is prematurely invalidated.'),
      '#description' => $this->t('Values under 120 will be used as 0'),
      '#default_value' => $config->get('anonymous_account')['inactivity_interval'],
    ];

    $form['anonymous_account']['cache_expiration'] = [
      '#type' => 'number',
      '#title' => $this->t('Field cache'),
      '#description' => $this->t('Duration of the Drupal cache for the field and, by inheritance,the entity.') . '<br/>' . $this->t('Leave empty to use inactivity level.'),
      '#default_value' => (isset($config->get('anonymous_account')['cache_expiration'])) ? $config->get('anonymous_account')['cache_expiration'] : NULL,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('luzmo_viewer.settings');

    /*
     * Remove empty collections when saving configurations.
     */
    $collections = $form_state->getValue('collections')['collection'];

    foreach ($collections as $k => $collection) {
      if (empty($collection)) {
        array_splice($collections, $k, 1);
      }
    }

    $config
      ->set('appserver', $form_state->getValue('appserver'))
      ->set('authkey', $form_state->getValue('authkey'))
      ->set('authtoken', $form_state->getValue('authtoken'))
      ->set('luzmo_js', $form_state->getValue('luzmo_js'))
      ->set('logged_account', $form_state->getValue('logged_account'))
      ->set('anonymous_account', $form_state->getValue('anonymous_account'))
      ->set('collections', $collections)
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addMoreCollectionCallback(array &$form, FormStateInterface $form_state) {
    return $form['collections'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOneCollection(array &$form, FormStateInterface $form_state) {
    $collections_count = $form_state->get('collections');
    $form_state->set('collections', $collections_count + 1);
    /*
     * Since our buildForm() method relies on the value of 'collections'
     * to generate 'collection' form elements,
     * we have to tell the form to rebuild.
     * If we don't do this, the form builder will not call buildForm().
     */
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['luzmo_viewer.settings'];
  }

}
