<?php

/**
 * @file
 */

/**
 * Alter dashboard_javascript authorisation settings.
 *
 * @param array $authorizationSettings
 *   The authorisation settings for luzmo.
 * @param string $dashboard_id
 *   The dashboard id.
 */
function hook_luzmo_viewer_dashboard_javascript_authorisation_alter(&$authorizationSettings, $dashboard_id) {
  if ($dashboard_id == '3c036b4b-ba64-419d-ae13-c4c5235adbfa') {
    $authorizationSettings['filters'] = [
      [
        "clause" => 'where',
        "origin" => 'initialization',
        "chart_id" => '240ae4e8-4af8-4a1b-8f8c-9b5eda45d670',
        "expression" => '? in ?',
        "value" => [
          'Demo',
        ],
      ],
    ];
  }
}
